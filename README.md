# Clean up borked Furios price export files

## Introduction

This script makes use of the [csvkit toolkit](https://csvkit.readthedocs.io/) and cleans up Furios price files that don't contain 2 lines for reduced prices but one line with the original (UVP) price in the `volume_prices` field.

## Usage

```bash
./duplicate FILENAME FOOTNOTE_TEXT
```

This will create an additional file with the same name but added `_clean` to its name. This file will contain the correct DEFAULT and ORIGINAL price rows for reduced products.

## Appendix

The `notepad.txt` file is my scratch pad to jot down things I try at the command line.