#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Wrong number of arguments!"
    echo "Usage: $0 FILENAME FOOTNOTE_TEXT"
    exit
fi

rm -f unchanged.csv original.csv reduced.csv

filename="$1"
footnote_text="$2"

bname=`basename $filename .csv`

nname="${bname}_new.csv"
oname="${bname}_new_out.csv"
clname="${bname}_clean.csv"
sname="${bname}_source.csv"
soname="${bname}_out.csv"

rm -f $nname $oname $clname $sname $soname

csvclean -K1 $filename
echo "abstract_sku,concrete_sku,price_type,store,currency,tax_value,value_gross,value_netto,volume_prices,footprint_text" > $sname
cat $soname >> $sname

csvsql --query "select abstract_sku, concrete_sku, 'DEFAULT' as price_type, store, currency, CAST(tax_value AS INTEGER) as tax_value, CAST(value_gross AS INTEGER) as value_gross, CAST(value_netto AS INTEGER) as value_netto, 0 as volume_prices,'' as footprint_text from prices where volume_prices = 0" --table prices $sname > unchanged.csv
csvsql --query "select abstract_sku, concrete_sku, 'DEFAULT' as price_type, store, currency, CAST(tax_value AS INTEGER) as tax_value, CAST(value_gross AS INTEGER) as value_gross, CAST(value_netto AS INTEGER) as value_netto, 0 as volume_prices,'' as footprint_text from prices where volume_prices > 0" --table prices $sname > reduced.csv
csvsql --query "select abstract_sku, concrete_sku, 'ORIGINAL' as price_type, store, currency, CAST(tax_value AS INTEGER) as tax_value, CAST(volume_prices AS INTEGER) as value_gross, CAST(ROUND(volume_prices/1.19) AS INTEGER) as value_netto, 0 as volume_prices,'$footnote_text' as footprint_text from prices where volume_prices > 0" --table prices $sname > original.csv

csvstack unchanged.csv original.csv reduced.csv > $nname
csvclean -K1 $nname
echo "abstract_sku,concrete_sku,price_type,store,currency,tax_value,value_gross,value_netto,price_data.volume_prices,price_data.footprint_text" > $clname
cat $oname >> $clname

rm -f $nname $oname $sname $soname unchanged.csv original.csv reduced.csv